#!/bin/sh

PACKAGES="\
    alsa-utils \
    bat \
    btop \
    dashbinsh \
    dunst \
    exa \
    `# gtk-theme-numix-solarized` \
    `# imagemagick` \
    lxappearance \
    man-db \
    neovim \
    nsxiv \
    `# nerd-fonts-fira-code` \
    pdftk \
    sof-firmware \
    starship \
    udisks2 \
    unclutter \
    xcape \
    xdotool \
    xorg-xset \
    zathura \
    zsh \
    zsh-autosuggestions \
    zsh-fast-syntax-highlighting \
    "

XDG_CONFIG_HOME="$HOME/.config"
XDG_DATA_HOME="$HOME/.local/share"
XDG_CACHE_HOME="$HOME/.cache"
CARGO_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/cargo"
HISTFILE="${XDG_CACHE_HOME:-$HOME/.cache}/zsh/history"

installpkgs() { paru --needed --noconfirm -S $PACKAGES ;}

touchhist() { mkdir -p "$(dirname $HISTFILE)" && touch $HISTFILE ;}

cleanhouse() {
    rm -rf $HOME/.bash*
    mv $HOME/.cargo $CARGO_HOME
}

installpkgs
touchhist
cleanhouse
