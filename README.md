# dotfiles

Azure Linux's dotfiles, managed by chezmoi.

## Getting started

Issue `chezmoi init --apply git@gitlab.oit.duke.edu:azure/dotfiles.git`.
